﻿using FlickrNet;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace FlickrMassUploadr
{
    class Program
    {
        // Store the Frob in a private variable
        private string tempFrob;
        private string ApiKey = "67e6123efc36512716bfaf12f09c8ca5";
        private string SharedSecret = "15681c04364e4b09";
        private Flickr flickr;

        static void Main(string[] args)
        {
            var p = new Program();
            p.Run();
        }

        public void Run()
        {
            // Authenticate for the user to Flickr
            this.StartAuth();
            Console.WriteLine("Please authorize this application from your web browser.");
            Console.WriteLine("Press enter when finished...");
            Console.ReadLine();
            var token = this.EndAuth();

            flickr = new Flickr(ApiKey, SharedSecret, token);

            // Request for the directory path
            Console.WriteLine(@"Please State Where to Upload Photos from: (ex. C:\users\johndoe\Pictures)");
            string rootOfFoldersToUpload = Console.ReadLine();

            var directories = Directory.GetDirectories(rootOfFoldersToUpload, "*", SearchOption.AllDirectories);

            // now we should iterate over all directories.  Any directory with pictures in it should be added.
            foreach (var directory in directories)
            {
                // if directory contains any photos, we should upload them, provided a set of the same name doesn't exist
                var files = Directory.GetFiles(directory).OrderBy(x => GetDateTakenFromImage(x));
                if (files.Count() > 0)
                {
                    this.UploadFiles(directory);
                }
            }

            Console.WriteLine("Photos have been uploaded.  Please view Log.txt to see a ");
            Console.WriteLine("list of any errors that have occured.  If no log is present,");
            Console.WriteLine("it is likely that no errors occured.");
            Console.ReadLine();
        }

        /// <summary>
        /// This method starts the authorization by getting the frob and launching the url for the user to authorize.
        /// </summary>
        private void StartAuth()
        {
            // Create Flickr instance
            Flickr flickr = new Flickr(ApiKey, SharedSecret);
            // Get Frob	
            tempFrob = flickr.AuthGetFrob();

            // Calculate the URL at Flickr to redirect the user to
            string flickrUrl = flickr.AuthCalcUrl(tempFrob, AuthLevel.Write);
            // The following line will load the URL in the users default browser.
            System.Diagnostics.Process.Start(flickrUrl);
        }

        /// <summary>
        /// This method is called after authorized.  It gets the token for the user and returns it to the caller.
        /// </summary>
        /// <returns>The token for this session/user</returns>
        private string EndAuth()
        {
            // Create Flickr instance
            Flickr flickr = new Flickr(ApiKey, SharedSecret);
            try
            {
                // use the temporary Frob to get the authentication
                Auth auth = flickr.AuthGetToken(tempFrob);
                // Store this Token for later usage, 
                // or set your Flickr instance to use it.
                Console.WriteLine("User authenticated successfully");
                Console.WriteLine("Authentication token is " + auth.Token);
                return auth.Token;
            }
            catch (FlickrException ex)
            {
                // If user did not authenticat your application 
                // then a FlickrException will be thrown.
                Console.WriteLine("User did not authenticate you");
                Console.WriteLine(ex.ToString());
            }
            return null;
        }

        /// <summary>
        /// This method is called by run and is used to upload all photos and create a set for them from a folder
        /// </summary>
        /// <param name="directory">The path on disk to a directory which you want uploaded.</param>
        private void UploadFiles(string directory)
        {
            var files = Directory.GetFiles(directory);

            var photosetName = new DirectoryInfo(directory).Name;

            Console.WriteLine("Creating Set: " + photosetName);

            Photoset photoset = null;
            foreach (var file in files)
            {
                Console.WriteLine("Uploading and Adding Photo: " + file);
                try
                {
                    // no reason to bother if it is a thumbs.db file.  We know these are unacceptable files
                    if (!Path.GetFileName(file).EndsWith(".db"))
                    {
                        photoset = UploadPhotoToFlickrAndPutInPhotoset(photosetName, photoset, file);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    File.AppendAllText("Log.txt", "File: " + file + "\r\n" + e.ToString() + "\r\n");
                }
            }
        }

        /// <summary>
        /// This method is a wrapped version of upload picture and photoset add photo. This was done to make it more resillient to transient web errors.
        /// </summary>
        /// <param name="photosetName">The text name of the photoset to be created/used</param>
        /// <param name="photoset">The photoset to be used for storage.  This is passed so we don't have to attempt to find the photoset after the initial search for the directory.</param>
        /// <param name="file">The file on disk that you wish to have uploaded</param>
        /// <returns>The path to this photoset to which you uploaded.</returns>
        private Photoset UploadPhotoToFlickrAndPutInPhotoset(string photosetName, Photoset photoset, string file)
        {
            string photoId = (string)RunWithRetry(() =>
                {
                    return flickr.UploadPicture(file, Path.GetFileNameWithoutExtension(file), "", "", true, true, true);
                });

            photoset = (Photoset)RunWithRetry(() =>
                {
                    // first loop, andd photoset
                    if (photoset == null)
                    {
                        photoset = CreateOrFindPhotoset(photosetName, photoId);
                    }
                    return photoset;
                });

            return (Photoset)RunWithRetry(() =>
                {
                    // if these match, the photo was added to the set as part of the createOrFindPhotoset.  Otherwise, we still need to add it.
                    if (photoset.PrimaryPhotoId != photoId)
                    {
                        flickr.PhotosetsAddPhoto(photoset.PhotosetId, photoId);
                    }

                    return photoset;
                });
        }

        /// <summary>
        /// This method attempts to find a photoset.  If the photoset is not found, it is created.
        /// </summary>
        /// <param name="photosetName"></param>
        /// <param name="photoId"></param>
        /// <returns></returns>
        private Photoset CreateOrFindPhotoset(string photosetName, string photoId)
        {
            // try to find a photoset by name
            var photosetList = flickr.PhotosetsGetList();
            foreach (var set in photosetList)
            {
                if (set.Title == photosetName)
                {
                    return set;
                }
            }

            // photoset was not found, so we will just create it...
            var photoset = flickr.PhotosetsCreate(photosetName, photoId);

            // there seems to be a bug where the full photoset isn't populated immediately.  If we request the photoset by id though, it does populate the remainder
            photoset = flickr.PhotosetsGetInfo(photoset.PhotosetId);

            return photoset;
        }

        
        /// <summary>
        /// Regular Expression used by <see cref="GetDateTakenFromImage"/> to process the DateTime.
        /// we init this once so that if the function is repeatedly called it isn't stressing the garbage man
        /// </summary>
        private static Regex dateTakenRegex = new Regex(":");

        /// <summary>
        /// This method retrieves the DateTime from the image attributes.
        /// This solution was presented on StackOverflow.  I have modified this method to suit my purposes.
        /// http://stackoverflow.com/questions/180030/how-can-i-find-out-when-a-picture-was-actually-taken-in-c-sharp-running-on-vista
        /// </summary>
        /// <param name="path">Path to the image.</param>
        /// <returns>The Date Taken DateTime.</returns>
        private static DateTime GetDateTakenFromImage(string path)
        {
            try
            {
                using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read))
                using (Image image = Image.FromStream(stream, false, false))
                {
                    PropertyItem propItem = image.GetPropertyItem(36867);
                    string dateTaken = dateTakenRegex.Replace(Encoding.UTF8.GetString(propItem.Value), "-", 2);
                    return DateTime.Parse(dateTaken);
                }
            }
            catch
            {
                return DateTime.MinValue;
            }
        }

        /// <summary>
        /// This method allows us to retry on failure.  This is particularly useful for web requests that sometimes fail just from timeout...
        /// </summary>
        /// <param name="func"></param>
        /// <returns></returns>
        private object RunWithRetry(Func<object> func, int retries = 5)
        {
            while (true)
            {
                try
                {
                    return func.Invoke();
                }
                catch (Exception e)
                {
                    if (--retries == 0)
                    {
                        throw;
                    }
                    else
                    {
                        File.AppendAllText("Log.txt", "WARNING: Failure on retry of function\r\n" + e.ToString());
                        Thread.Sleep(1000);
                    }
                }
            }
        }
    }
}
